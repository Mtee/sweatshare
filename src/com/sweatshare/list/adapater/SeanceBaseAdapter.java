package com.sweatshare.list.adapater;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sweatshare.api.modele.ISeance;
import com.sweatshare.view.SeanceItemView;

public class SeanceBaseAdapter extends BaseAdapter {

	private List<ISeance> seances;
	private Context context;
	
	public SeanceBaseAdapter(Context context){
		seances = new ArrayList<ISeance>();
		this.context = context;
	}
	
	public void add(ISeance seance){
		this.seances.add(seance);
	}
	
	public void delete( ISeance seance ){
		this.seances.remove(seance);
	}
	
	@Override
	public int getCount() {
		return seances.size();
	}

	@Override
	public Object getItem(int arg0) {
		return seances.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		SeanceItemView view = new SeanceItemView(seances.get(arg0), context);
		return view.getView(arg1, arg2);
	}

}
