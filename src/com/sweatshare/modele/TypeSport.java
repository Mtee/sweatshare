package com.sweatshare.modele;

import org.json.JSONException;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.sweatshare.api.modele.IJsonSerializable;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.communication.JsonSerializer;

public class TypeSport implements ITypeSport, IJsonSerializable {
	
	@Expose
	private Long id;
	@Expose
	private String libelle;

	public TypeSport(Parcel source) {
		String serializedJson = source.readString();
		TypeSport tsport = (TypeSport) JsonSerializer.deserialize(TypeSport.class, serializedJson);
		id = tsport.getId();
		libelle = tsport.getLibelle();
	}
	
	public TypeSport(){
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			String userJson = JsonSerializer.serialize(this).toString();
			dest.writeString(userJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static final Parcelable.Creator<TypeSport> CREATOR = new Parcelable.Creator<TypeSport>()
	{
	    @Override
	    public TypeSport createFromParcel(Parcel source)
	    {
	        return new TypeSport(source);
	    }

	    @Override
	    public TypeSport[] newArray(int size)
	    {
		return new TypeSport[size];
	    }
	};
	
}
