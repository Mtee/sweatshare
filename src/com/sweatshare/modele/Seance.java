package com.sweatshare.modele;

import java.util.Date;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;

public class Seance implements ISeance {

	private Long id;
	private Integer duree;
	private Date date;
	private Date heureDepart;
	private Date heureArrivee;
	private IParcours parcours;
	private ITypeSport sport;
	private IMeteo meteo;
	private IUtilisateur utilisateur;
	
	public Seance(Parcel source) {
		String serializedJson = source.readString();
		ISeance seance = (ISeance) JsonSerializer.deserialize(Seance.class, serializedJson);
		this.id = seance.getId();
		this.duree = seance.getDuree();
		this.date = seance.getDate();
		this.heureDepart = seance.getHeureDepart();
		this.heureArrivee = seance.getHeureArrivee();
		this.parcours = seance.getParcours();
		this.sport = seance.getSport();
		this.meteo = seance.getMeteo();
		this.utilisateur = seance.getUtilisateur();
	}
	
	public Seance() {
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			String seanceJson = JsonSerializer.serialize(this).toString();
			dest.writeString(seanceJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	public static final Parcelable.Creator<Seance> CREATOR = new Parcelable.Creator<Seance>()
	{
	    @Override
	    public Seance createFromParcel(Parcel source)
	    {
	        return new Seance(source);
	    }

	    @Override
	    public Seance[] newArray(int size)
	    {
		return new Seance[size];
	    }
	};
	
	public Integer getDuree() {
		return duree;
	}
	
	public void setDuree(Integer duree) {
		this.duree = duree;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getHeureDepart() {
		return heureDepart;
	}
	
	public void setHeureDepart(Date heureDepart) {
		this.heureDepart = heureDepart;
	}
	
	public Date getHeureArrivee() {
		return heureArrivee;
	}
	
	public void setHeureArrivee(Date heureArrivee) {
		this.heureArrivee = heureArrivee;
	}

	public IParcours getParcours() {
		return parcours;
	}

	public void setParcours(IParcours parcours) {
		this.parcours = parcours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ITypeSport getSport() {
		return sport;
	}

	public void setSport(ITypeSport sport) {
		this.sport = sport;
	}

	public IMeteo getMeteo() {
		return meteo;
	}

	public void setMeteo(IMeteo meteo) {
		this.meteo = meteo;
	}

	public IUtilisateur getUtilisateur() {
		return utilisateur;
	}

	@Override
	public void setUtilisateur(IUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	@Override
	public int describeContents() {
		return 0;
	}
}