package com.sweatshare.modele;

import java.util.Date;

import com.sweatshare.api.modele.ICardio;
import com.sweatshare.api.modele.IPointGPS;

public class PointGPS implements IPointGPS {

	private Long id;
	private Float x;
	private Float y;
	private Float z;
	private Date dateReleve;
	
	private ICardio frequence;

	public PointGPS(){}
	
	public Float getX() {
		return x;
	}

	public void setX(Float x) {
		this.x = x;
	}

	public Float getY() {
		return y;
	}

	public void setY(Float y) {
		this.y = y;
	}

	public Float getZ() {
		return z;
	}

	public void setZ(Float z) {
		this.z = z;
	}

	public Date getDateReleve() {
		return dateReleve;
	}

	public void setDateReleve(Date dateReleve) {
		this.dateReleve = dateReleve;
	}

	public ICardio getFrequence() {
		return frequence;
	}

	public void setFrequence(ICardio frequence) {
		this.frequence = frequence;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
