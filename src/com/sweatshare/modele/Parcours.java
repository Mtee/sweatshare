package com.sweatshare.modele;

import java.util.Set;

import org.json.JSONException;

import android.os.Parcel;
import android.os.Parcelable;

import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.IPointGPS;
import com.sweatshare.communication.JsonSerializer;

public class Parcours implements IParcours {

	private Long id;
	private Set<IPointGPS> gps;
	private float distance = -1;

	public Parcours(Parcel source) {
		String serializedJson = source.readString();
		IParcours parcours = (IParcours) JsonSerializer.deserialize(
				Parcours.class, serializedJson);
		this.id = parcours.getId();
		this.gps = parcours.getGps();
		this.distance = parcours.getDistance();
	}
	
	public Parcours(){}

	public Set<IPointGPS> getGps() {
		return gps;
	}

	public void setGps(Set<IPointGPS> gps) {
		this.gps = gps;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public float getDistance() {
		return distance;
	}

	@Override
	public void setSeance(float Length) {
		distance = Length;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			String parcoursJson = JsonSerializer.serialize(this).toString();
			dest.writeString(parcoursJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static final Parcelable.Creator<Parcours> CREATOR = new Parcelable.Creator<Parcours>() {
		@Override
		public Parcours createFromParcel(Parcel source) {
			return new Parcours(source);
		}

		@Override
		public Parcours[] newArray(int size) {
			return new Parcours[size];
		}
	};

	@Override
	public void setPointGps(Set<IPointGPS> points) {
		this.gps = points;
	}

	@Override
	public void setDistance(float distance) {
		this.distance = distance;
	}
}
