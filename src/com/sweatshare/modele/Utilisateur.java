package com.sweatshare.modele;

import java.util.Date;
import java.util.Set;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;

public class Utilisateur implements IUtilisateur {
	
	private Long  id;
	private String nom;
	private String password;
	private String prenom;
	private String pseudo;
	private Date dateNaissance;
	private Float poids; // kg
	private Integer taille; // cm
	private Boolean female; // true = female, false = male
	private String mail;
	private String avatarUrl;
	private Set<ISeance> seances;
	
	public Utilisateur(){}
	
	public Utilisateur(String nom, String prenom, String pseudo)
	{
		this.id = null;
		this.nom = nom;
		this.prenom = prenom;
		this.pseudo = pseudo;
	}
	
	public Utilisateur(Parcel source) {
		String serializedJson = source.readString();
		IUtilisateur utilisateur = (IUtilisateur) JsonSerializer.deserialize(Utilisateur.class, serializedJson);
		this.id = utilisateur.getId();
		this.avatarUrl = utilisateur.getAvatarUrl();
		this.dateNaissance = utilisateur.getDateNaissance();
		this.female = utilisateur.isFemale();
		this.mail = utilisateur.getMail();
		this.nom = utilisateur.getNom();
		this.password = utilisateur.getPassword();
		this.poids = utilisateur.getPoids();
		this.prenom = utilisateur.getPrenom();
		this.pseudo = utilisateur.getPseudo();
		this.seances = utilisateur.getSeances();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getPseudo() {
		return pseudo;
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	public Date getDateNaissance() {
		return dateNaissance;
	}
	
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	
	public Float getPoids() {
		return poids;
	}
	
	public void setPoids(Float poids) {
		this.poids = poids;
	}
	
	public Integer getTaille() {
		return taille;
	}
	
	public void setTaille(Integer taille) {
		this.taille = taille;
	}
	
	public Boolean isFemale() {
		return female;
	}
	
	public void setFemale(Boolean sexe) {
		this.female = sexe;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getAvatarUrl() {
		return avatarUrl;
	}
	
	public void seatAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public Set<ISeance> getSeances() {
		return seances;
	}

	public void setSeances(Set<ISeance> seances) {
		this.seances = seances;
	}
	
	public void addSeance( Seance seance )
	{
		this.seances.add( seance );
	}
	
	public void removeSeance( Seance seance )
	{
		this.seances.remove(seance);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean isLogged() {
		return this.id != null;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			String userJson = JsonSerializer.serialize(this).toString();
			dest.writeString(userJson);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static final Parcelable.Creator<Utilisateur> CREATOR = new Parcelable.Creator<Utilisateur>()
	{
	    @Override
	    public Utilisateur createFromParcel(Parcel source)
	    {
	        return new Utilisateur(source);
	    }

	    @Override
	    public Utilisateur[] newArray(int size)
	    {
		return new Utilisateur[size];
	    }
	};

	@Override
	public void setAvatarUrl(String url) {
		// TODO Auto-generated method stub
		
	}
	
}
