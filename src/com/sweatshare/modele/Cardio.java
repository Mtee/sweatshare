package com.sweatshare.modele;

import com.sweatshare.api.modele.ICardio;

public class Cardio implements ICardio {

	private Long id;
	private Float frequence;
	
	public Cardio(){}
	
	public Cardio(Float frequence) {
		this.frequence = frequence;
	}

	public Float getFrequence() {
		return frequence;
	}

	public void setFrequence(Float frequence) {
		this.frequence = frequence;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
