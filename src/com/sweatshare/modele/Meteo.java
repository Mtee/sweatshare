package com.sweatshare.modele;

import org.json.JSONException;

import android.os.Parcel;
import android.os.Parcelable;

import com.sweatshare.api.modele.IJsonSerializable;
import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;

public class Meteo implements IMeteo, IJsonSerializable {
	
	private Long id;
	private Float temp; // celsius
	private Float vitesseVent; // km/h
	private String libelle; // rainy, sunny, etc
	
	public Meteo(Parcel source) {
		String serializedJson = source.readString();
		IMeteo meteo = (IMeteo) JsonSerializer.deserialize(Meteo.class, serializedJson);
		id = meteo.getId();
		temp = meteo.getTemp();
		vitesseVent = meteo.getVitesseVent();
		libelle = meteo.getLibelle();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Float getTemp() {
		return temp;
	}
	
	public void setTemp(Float temp) {
		this.temp = temp;
	}
	
	public Float getVitesseVent() {
		return vitesseVent;
	}
	
	public void setVitesseVent(Float vitesseVent) {
		this.vitesseVent = vitesseVent;
	}
	
	public String getLibelle() {
		return libelle;
	}
	
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		try {
			String json = JsonSerializer.serialize(this).toString();
			dest.writeString(json);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public static final Parcelable.Creator<Meteo> CREATOR = new Parcelable.Creator<Meteo>()
	{
	    @Override
	    public Meteo createFromParcel(Parcel source)
	    {
	        return new Meteo(source);
	    }

	    @Override
	    public Meteo[] newArray(int size)
	    {
		return new Meteo[size];
	    }
	};
}