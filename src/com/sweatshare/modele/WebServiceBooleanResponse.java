package com.sweatshare.modele;

import com.sweatshare.api.modele.IBooleanResponse;

public class WebServiceBooleanResponse implements IBooleanResponse{

	private boolean result;

	@Override
	public boolean getResult() {
		return result;
	}

	@Override
	public void setResult(Boolean result) {
		this.result = result;
	}
	
}
