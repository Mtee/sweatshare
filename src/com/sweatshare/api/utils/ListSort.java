package com.sweatshare.api.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.sweatshare.api.modele.IPointGPS;
import com.sweatshare.api.modele.ISeance;

public class ListSort {

	public static enum SORT{
		DESC, ASC
	};
	
	public static void sortGpsByDate( List<IPointGPS> gps, final SORT sort ){
		Collections.sort(gps, new Comparator<IPointGPS>() {
			@Override
			public int compare(IPointGPS lhs, IPointGPS rhs) {
				if( lhs.getDateReleve().before(rhs.getDateReleve()) ){
					return sort == SORT.ASC ? -1 : 1;
				}
				else if( lhs.getDateReleve().after(rhs.getDateReleve()) ){
					return sort == SORT.ASC ? 1 : -1;
				}
				return 0;
			}
		});
	}
	
	public static void sortSeanceByDate( List<ISeance> seances, final SORT sort ){
		Collections.sort(seances, new Comparator<ISeance>() {
			@Override
			public int compare(ISeance lhs, ISeance rhs) {
				
				if( lhs.getDate() == null ) return sort == SORT.ASC ? -1 : 1;
				if( rhs.getDate() == null ) return sort == SORT.ASC ? 1 : -11;
				
				if( lhs.getDate().before(rhs.getDate()) ){
					return sort == SORT.ASC ? -1 : 1;
				}
				else if( lhs.getDate().after(rhs.getDate()) ){
					return sort == SORT.ASC ? 1 : -1;
				}
				return 0;
			}
		});
	}
	
}
