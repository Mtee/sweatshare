package com.sweatshare.api.communication;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.Response;

public interface IWebServiceReceiverArray extends Response.ErrorListener, Response.Listener<JSONArray> {

}
