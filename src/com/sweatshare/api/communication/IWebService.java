package com.sweatshare.api.communication;

import org.json.JSONObject;

import com.android.volley.Response.Listener;
import com.sweatshare.api.modele.IJsonSerializable;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.IUtilisateur;

public interface IWebService {

	public void getUrl();
	public String setUrl(String url);
	
	public void save( IJsonSerializable entity );
	
	//User
	public void login( IUtilisateur user, IWebServiceReceiverObject receiver );
	public void createUser( IUtilisateur utilisateur, IWebServiceReceiverObject receiver );
	public void loadUser( IUtilisateur user, Listener<JSONObject> listener  );
	public void updateUser( IUtilisateur user, Listener<JSONObject> listener  );
	
	//Seances
	public void createSeance( IUtilisateur utilisateur, ISeance seance, IWebServiceReceiverObject receiver );
	public void getSeances(IUtilisateur utilisateur, IWebServiceReceiverArray receiver);
	
	//Meteo
	public void getAvailableMeteos(IWebServiceReceiverArray receiver );
	
	//Sport
	public void getAvailableSports( IWebServiceReceiverArray receiver );
	
}
