package com.sweatshare.api.communication;

import org.json.JSONObject;

import com.android.volley.Response;
import com.google.gson.JsonObject;

public interface IWebServiceReceiverObject extends Response.ErrorListener, Response.Listener<JSONObject> {

}
