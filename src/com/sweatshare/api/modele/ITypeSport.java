package com.sweatshare.api.modele;

import android.os.Parcelable;

public interface ITypeSport extends Parcelable{
	
	public static final String TYPESPORT_EXTRA = "TYPESPORT_EXTRA";
	
	public Long getId();
	public void setId(Long id);
	public String getLibelle();
	public void setLibelle(String libelle);
}
