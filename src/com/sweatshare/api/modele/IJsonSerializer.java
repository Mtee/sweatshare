package com.sweatshare.api.modele;


public interface IJsonSerializer {

	public String serialize( IJsonSerializable object );
	public IJsonSerializable deserialize( Class<IJsonSerializable> target, String json);
	
}
