package com.sweatshare.api.modele;

import java.util.Set;

import android.os.Parcelable;

public interface IParcours extends IJsonSerializable, Parcelable{

	String EXTRA_PARCOURS = "PARCOURS";
	public Set<IPointGPS> getGps();
	public void setPointGps( Set<IPointGPS> points );
	public float getDistance();
	public void setDistance( float distance );
	public void setSeance(float Length);
	public Long getId();
}
