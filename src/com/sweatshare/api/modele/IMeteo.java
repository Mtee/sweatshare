package com.sweatshare.api.modele;

import android.os.Parcelable;

public interface IMeteo extends Parcelable{

	public static final String METEO_EXTRA = "EXTRA_METEO";
	
	public Long getId();
	public void setId(Long id);
	public Float getTemp();
	public void setTemp(Float temp);
	public Float getVitesseVent();
	public void setVitesseVent(Float temp);
	public String getLibelle();
	public void setLibelle(String libelle);
	
}
