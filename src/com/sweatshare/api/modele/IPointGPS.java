package com.sweatshare.api.modele;

import java.util.Date;

public interface IPointGPS{

	public Date getDateReleve();
	public void setDateReleve(Date date);
	public ICardio getFrequence();
	public void setFrequence(ICardio frequence);
	public Float getX();
	public void setX(Float x);
	public Float getY();
	public void setY(Float y);
	public Float getZ();
	public void setZ(Float z);
	
}
