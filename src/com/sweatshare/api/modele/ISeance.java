package com.sweatshare.api.modele;

import java.util.Date;

import android.os.Parcelable;

public interface ISeance extends IJsonSerializable, Parcelable{

	public static final String EXTRA_SEANCE = "SEANCE";
	public static final String EXTRA_SEANCE_SAVED = "LAST_SEANCE_SAVED";
	
	public Long getId();
	public Date getDate();
	public void setDate(Date date);
	public Integer getDuree();
	public void setDuree(Integer duree);
	public Date getHeureArrivee();
	public void setHeureArrivee(Date heureArrivee);
	public Date getHeureDepart();
	public void setHeureDepart(Date heureDepart);
	public IMeteo getMeteo();
	public void setMeteo(IMeteo meteo);
	public IParcours getParcours();
	public void setParcours(IParcours parcours);
	public ITypeSport getSport();
	public void setSport(ITypeSport sport);
	public IUtilisateur getUtilisateur();
	public void setUtilisateur(IUtilisateur utilisateur);
}
