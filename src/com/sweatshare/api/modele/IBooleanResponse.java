package com.sweatshare.api.modele;

public interface IBooleanResponse {

	public boolean getResult();
	public void setResult( Boolean response );
	
}
