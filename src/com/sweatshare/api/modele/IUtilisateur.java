package com.sweatshare.api.modele;

import java.util.Date;
import java.util.Set;

import android.os.Parcelable;

public interface IUtilisateur extends IJsonSerializable, Parcelable{

	public static final String USER_EXTRA = "USER";
	
	public Long getId();
	public String getAvatarUrl();
	public void setAvatarUrl(String url);
	public Date getDateNaissance();
	public void setDateNaissance(Date dateNaissance);
	public Boolean isFemale();
	public void setFemale(Boolean female);
	public String getMail();
	public void setMail(String mail);
	public String getNom();
	public void setNom(String nom);
	public String getPrenom();
	public void setPrenom(String prenom);
	public Float getPoids();
	public void setPoids(Float poids);
	public String getPseudo();
	public void setPseudo(String pseudo);
	public Integer getTaille();
	public void setTaille(Integer taille);
	public String getPassword();
	public void setPassword(String password);
	public boolean isLogged();
	public Set<ISeance> getSeances();
	
}
