package com.sweatshare.view;

import java.text.DateFormat;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.sweatshare.R;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.communication.VolleySingleton;
import com.sweatshare.communication.WebService;

public class SeanceItemView {

	private static final int LAYOUT = R.layout.item_list_seance;
	private ISeance seance;
	private Context context;

	public SeanceItemView(ISeance seance, Context ctx){
		this.seance = seance;
		this.context = ctx;
	}
	
	public View getView( View v, ViewGroup root ){
		
		DateFormat df = DateFormat.getDateInstance();
		VolleySingleton volley = VolleySingleton.getInstance();
		
		if( v == null ){
			v = LayoutInflater.from(context).inflate(LAYOUT, null);
		}
		
		TextView txt = (TextView) v.findViewById(R.id.item_list_seance_date_seance);
		if( seance.getDate() != null ){
			txt.setText( df.format(seance.getDate()) );
		}
		
		ImageView img = (ImageView) v.findViewById(R.id.item_list_seance_type_sport);
		ImageSeanceLoader imgloader = new ImageSeanceLoader(img);
		
		if( seance.getSport() != null ){
			volley.getImageLoader().get(buildUrlSport(), imgloader );
		}
		if( seance.getMeteo() != null ){
			img = (ImageView) v.findViewById(R.id.item_list_seance_meteo);
			imgloader = new ImageSeanceLoader(img);
			volley.getImageLoader().get(buildUrlMete(), imgloader );
		}
		if( seance.getParcours() != null ){
			TextView txtDist = (TextView) v.findViewById(R.id.item_list_seance_distance);
			txtDist.setText(String.valueOf(seance.getParcours().getDistance()) + "m");
		}
		return v;
	}
	
	private String buildUrlSport(){
		return WebService.URL_IMAGES + seance.getSport().getLibelle() + ".png";
	}
	
	private String buildUrlMete(){
		return WebService.URL_IMAGES + seance.getMeteo().getLibelle() + ".png";
	}
	
	private class ImageSeanceLoader implements ImageListener{

		private ImageView img;
		
		public ImageSeanceLoader( ImageView img ){
			this.img = img;
		}
		
		@Override
		public void onErrorResponse(VolleyError arg0) {
			img.setImageResource(R.drawable.error_connexion);
			Log.e("SWEATSHARE", arg0.getMessage() + "");
		}

		@Override
		public void onResponse(ImageContainer container, boolean res) {
			img.setImageBitmap( container.getBitmap() );
		}	
	}
}
