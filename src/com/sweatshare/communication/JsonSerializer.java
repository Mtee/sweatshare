package com.sweatshare.communication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;
import com.sweatshare.api.modele.IBooleanResponse;
import com.sweatshare.api.modele.IJsonSerializable;
import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.IPointGPS;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.modele.Meteo;
import com.sweatshare.modele.Parcours;
import com.sweatshare.modele.PointGPS;
import com.sweatshare.modele.Seance;
import com.sweatshare.modele.TypeSport;
import com.sweatshare.modele.Utilisateur;
import com.sweatshare.modele.WebServiceBooleanResponse;

public class JsonSerializer{

	public static JSONObject serialize(IJsonSerializable object) throws JSONException {
		GsonBuilder builder = new GsonBuilder(); 

		// Register an adapter to manage the date types as long values 
		builder.registerTypeAdapter(Date.class, new DateSerializer());
		Gson gson = builder.create();
		return new JSONObject(gson.toJson(object).toString());
	}

	public static Object deserialize(Class<?> target, String json) {
		// Creates the json object which will manage the information received 
		GsonBuilder builder = new GsonBuilder(); 

		builder.registerTypeAdapter( ITypeSport.class , new ITypeSportDeserializer());
		builder.registerTypeAdapter( IUtilisateur.class , new IUtilisateurDeserializer());
		builder.registerTypeAdapter( IMeteo.class, new IMeteoDeserializer());
		builder.registerTypeAdapter( IBooleanResponse.class, new IBooleanResponseInstanceCreator());
		builder.registerTypeAdapter( IParcours.class, new IParcoursDeserializer());
		builder.registerTypeAdapter( IPointGPS.class, new IPointDeserializer());
		
		// Register an adapter to manage the date types as long values 
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
		@Override
		public Date deserialize(JsonElement json, Type arg1,
				JsonDeserializationContext arg2) throws JsonParseException {
			 		return new Date(json.getAsJsonPrimitive().getAsLong()); 
				} 
		});
		
		Gson gson = builder.create();
		return gson.fromJson(json, target);
	}
	
	public static List<?> deserialize( Class<?> target, JSONArray json ){
		List<Object> returned = new ArrayList<Object>();
		
		// Creates the json object which will manage the information received 
		GsonBuilder builder = new GsonBuilder(); 

		// Register an adapter to manage the date types as long values 
		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
		@Override
		public Date deserialize(JsonElement json, Type arg1,
				JsonDeserializationContext arg2) throws JsonParseException {
			 		return new Date(json.getAsJsonPrimitive().getAsLong()); 
				} 
		});
		
		//Add instance creator to instanciate Interface using implementation
		builder.registerTypeAdapter( ITypeSport.class , new ITypeSportDeserializer());
		builder.registerTypeAdapter( ISeance.class , new ISeanceDeserializer());
		builder.registerTypeAdapter( IUtilisateur.class , new IUtilisateurDeserializer());
		builder.registerTypeAdapter( IMeteo.class, new IMeteoDeserializer());
		builder.registerTypeAdapter( IParcours.class, new IParcoursDeserializer());
		builder.registerTypeAdapter( IPointGPS.class, new IPointDeserializer());
		builder.registerTypeAdapter( IBooleanResponse.class, new IBooleanResponseInstanceCreator());
		
		for( int i = 0 ; i < json.length() ; i++ ){
			Gson gson = builder.create();
			try {
				returned.add( (target.cast(gson.fromJson(json.getJSONObject(i).toString(), target)) ));
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return returned;
	}

	private static class ITypeSportDeserializer implements JsonDeserializer<ITypeSport>{
		@Override
		public ITypeSport deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			builder.registerTypeAdapter( IPointGPS.class , new IPointDeserializer());
			Gson gson = builder.create();
			
			return gson.fromJson( element , TypeSport.class );
		}
	}
	
	private static class ISeanceDeserializer implements JsonDeserializer<ISeance>{
		@Override
		public ISeance deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			builder.registerTypeAdapter( IParcours.class , new IParcoursDeserializer());
			builder.registerTypeAdapter( IPointGPS.class , new IPointDeserializer());
			Gson gson = builder.create();
			
			return gson.fromJson( element , Seance.class );
		}
	}
	
	private static class IUtilisateurDeserializer implements JsonDeserializer<IUtilisateur>{
		@Override
		public IUtilisateur deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			builder.registerTypeAdapter( IParcours.class , new IParcoursDeserializer());
			builder.registerTypeAdapter( IPointGPS.class , new IPointDeserializer());
			Gson gson = builder.create();
			
			return gson.fromJson( element , Utilisateur.class );
		}
	}
	
	private static class IParcoursDeserializer implements JsonDeserializer<IParcours>{
		@Override
		public IParcours deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			builder.registerTypeAdapter( IPointGPS.class , new IPointDeserializer());
			
			Gson gson = builder.create();
			
			return gson.fromJson( element , Parcours.class );
		}
	}
	
	private static class IPointDeserializer implements JsonDeserializer<IPointGPS>{
		@Override
		public IPointGPS deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			Gson gson = builder.create();
			
			return gson.fromJson( element , PointGPS.class );
		}
	}
	
	private static class IMeteoDeserializer implements JsonDeserializer<IMeteo>{

		@Override
		public IMeteo deserialize(JsonElement element, Type type,
				JsonDeserializationContext context) throws JsonParseException {
			GsonBuilder builder = new GsonBuilder(); 
			builder.registerTypeAdapter( Date.class , new DateDeserialize());
			Gson gson = builder.create();
			
			return gson.fromJson( element , Meteo.class );
		}
	}
	
	private static class DateDeserialize implements JsonDeserializer<Date>{

		@Override
		public Date deserialize(JsonElement json, Type arg1,
				JsonDeserializationContext arg2) throws JsonParseException {
			return new Date(json.getAsJsonPrimitive().getAsLong()); 
		}
	}
	
	private static class DateSerializer implements com.google.gson.JsonSerializer<Date>{
		@Override
		public JsonElement serialize(Date date, Type type,
				JsonSerializationContext arg2) {
			return new JsonPrimitive(date.getTime());
		}
	};
	
	private static class IBooleanResponseInstanceCreator implements InstanceCreator<IBooleanResponse>{
		@Override
		public IBooleanResponse createInstance(Type arg0) {
			return new WebServiceBooleanResponse();
		}
	}
}
