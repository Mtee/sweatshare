package com.sweatshare.communication;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverArray;
import com.sweatshare.api.communication.IWebServiceReceiverObject;
import com.sweatshare.api.modele.IJsonSerializable;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.IUtilisateur;

public class WebService implements IWebService{

	public static final String URL_API = "http://buildandrun.aws.af.cm/api/";
	public static final String URL_IMAGES = "http://buildandrun.aws.af.cm/images/";
	private RequestQueue mRequestQueue;
	private static WebService webService;
	
//	private WebService( Context context ){
//		mRequestQueue = Volley.newRequestQueue(context);
//	}
	
	private WebService(){
		VolleySingleton volley = VolleySingleton.getInstance();
		mRequestQueue = volley.getRequestQueue();
	}
	
	public static WebService getInstance()
	{
		if( webService == null ) webService = new WebService();
		return webService;
	}
	
	public void save(IJsonSerializable entity) {
		
	}

	public void login(IUtilisateur user, IWebServiceReceiverObject receiver ){
		String url = URL_API + "utilisateurs/utilisateur/connect";
		sendPost(user, url, receiver);
	}
		

	public void createUser( IUtilisateur user, IWebServiceReceiverObject receiver ){
		String url = URL_API + "utilisateurs/utilisateur/create";
		sendPost( user, url, receiver );
	}
	
	@Override
	public void loadUser(IUtilisateur user, Listener<JSONObject> listener ) {
		
	}

	@Override
	public void updateUser(IUtilisateur user, Listener<JSONObject> listener ) {
		
	}

	
	
	@Override
	public void getUrl() {
		
	}

	@Override
	public String setUrl(String url) {
		return null;
	}
	
	private void sendPost( IJsonSerializable serializableObject, String url, IWebServiceReceiverObject receiver )
	{
		JsonObjectRequest request;
		try {
			mRequestQueue.getCache().clear();
			Log.i("SWEATSHARE", "Data send : " + JsonSerializer.serialize(serializableObject));
			request = new JsonObjectRequest(Request.Method.POST,
						url, JsonSerializer.serialize(serializableObject),
						receiver, receiver);
			mRequestQueue.add(request);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void sendGet( IJsonSerializable serializableObject, String url, IWebServiceReceiverObject receiver )
	{
		JsonObjectRequest request;
		try {
			mRequestQueue.getCache().clear();
			Log.i("SWEATSHARE", "Data send : " + JsonSerializer.serialize(serializableObject));
			request = new JsonObjectRequest(Request.Method.GET,
						url, JsonSerializer.serialize(serializableObject),
						receiver, receiver);
			mRequestQueue.add(request);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void sendGetWithoutData( String url, IWebServiceReceiverObject receiver )
	{
		JsonObjectRequest request;
		mRequestQueue.getCache().clear();
		Log.i("SWEATSHARE", "Query : " + url);
		request = new JsonObjectRequest(Request.Method.GET, url, null, receiver, receiver);
		mRequestQueue.add(request);
	}
	
	private void sendGetWithoutData( String url, IWebServiceReceiverArray receiver )
	{
		JsonArrayRequest request;
		mRequestQueue.getCache().clear();
		Log.i("SWEATSHARE", "Query : " + url);
		request = new JsonArrayRequest(url, receiver, receiver);
		mRequestQueue.add(request);
	}
	
	@Override
	public void getSeances(IUtilisateur utilisateur, IWebServiceReceiverArray receiver) {
		String url = URL_API + "seances/utilisateur/"+utilisateur.getId(); 
		sendGetWithoutData(url, receiver);	
	}

	@Override
	public void createSeance(IUtilisateur utilisateur, ISeance seance, IWebServiceReceiverObject receiver)
	{
		seance.setUtilisateur(utilisateur);
		sendPost(seance, URL_API + "seances/seance/create" , receiver);
	}

	@Override
	public void getAvailableMeteos(IWebServiceReceiverArray receiver) {
		sendGetWithoutData( URL_API + "meteos/all", receiver);
	}
	
	@Override
	public void getAvailableSports(IWebServiceReceiverArray receiver) {
		sendGetWithoutData( URL_API + "sports/all", receiver);
	}
	
}
