package com.sweatshare.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.R;
import com.sweatshare.android.view.GifDecoderView;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverArray;
import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.api.utils.ListSort;
import com.sweatshare.api.utils.ListSort.SORT;
import com.sweatshare.carousel.Carousel;
import com.sweatshare.carousel.CarouselAdapter;
import com.sweatshare.carousel.CarouselAdapter.OnItemClickListener;
import com.sweatshare.carousel.CarouselItem;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Meteo;
import com.sweatshare.modele.Seance;
import com.sweatshare.modele.TypeSport;

public class HomeContainerActivity extends FragmentActivity implements OnItemClickListener{
	
	private IUtilisateur user;
	private Carousel mCarousel;
	
	private List<ISeance> seances;
	private List<ITypeSport> sports;
	private List<IMeteo> meteos;
	
	private ISeance speedy;
	private ISeance farAway;
	private ISeance chrono;
	
	private boolean loaded;
	
	private GifDecoderView mGifView;
	private Dialog mDialog;
	private Intent intent; 
	
	private static final String BEST_SPEED = "Best speed" ;
	private static final String BEST_LENGTH = "Best way";
	private static final String BEST_CHRONO = "Best Time";
	
	public static final int REQUEST_MAPS = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.creation_seance_fragment);
		
		loaded = false;
		
		user = (IUtilisateur) getIntent().getExtras().get(IUtilisateur.USER_EXTRA);
		
		IWebService ws = WebService.getInstance();
		ws.getSeances(user, new OnSeanceReceived());
		
		ws.getAvailableSports(new OnSportReceive());
		ws.getAvailableMeteos( new OnMeteoReceived() );
		
		
		mCarousel = (Carousel) findViewById(R.id.carousel);
		init(mCarousel);
		mCarousel.setOnItemClickListener(this);
		
		ScrollView scroll = (ScrollView) findViewById(R.id.creation_seance_fragment_scrollview);
		scroll.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return !loaded;
			}
		});
		displayLoading();
	}

	private void init( Carousel carousel )
	{
		CarouselItem item = new CarouselItem(getApplicationContext());
		item.setText(BEST_SPEED);
		Drawable best = getResources().getDrawable(R.drawable.best_speed);
		Bitmap logo = ((BitmapDrawable) best).getBitmap();
		item.setImageBitmap(logo);
		carousel.addItem(item);

		item = new CarouselItem(getApplicationContext());
		item.setText(BEST_CHRONO);
		best = getResources().getDrawable(R.drawable.timer);
		logo = ((BitmapDrawable) best).getBitmap();
		item.setImageBitmap(logo);
		carousel.addItem(item);
		
		item = new CarouselItem(getApplicationContext());
		item.setText(BEST_LENGTH);
		best = getResources().getDrawable(R.drawable.award);
		logo = ((BitmapDrawable) best).getBitmap();
		item.setImageBitmap(logo);
		carousel.addItem(item);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void showHistory(View v) {
		Intent intent = new Intent(this, ShowHistoryActivity.class);
		intent.putExtra(IUtilisateur.USER_EXTRA, user);
		intent.putParcelableArrayListExtra(ISeance.EXTRA_SEANCE,
					(ArrayList<? extends Parcelable>) seances );
		startActivity(intent);
	}

	public void showMeteo(View v) {
		Intent intent = new Intent(this, ShowMeteoActivity.class);
		intent.putExtra(IUtilisateur.USER_EXTRA, user);
		startActivity(intent);
	}

	public void startRun(View v) {
		Intent intent = new Intent(this, MapsTrackingActivity.class);
		intent.putExtra(IUtilisateur.USER_EXTRA, user);
		intent.putParcelableArrayListExtra(ITypeSport.TYPESPORT_EXTRA,  
				(ArrayList<? extends Parcelable>) sports );
		intent.putParcelableArrayListExtra(IMeteo.METEO_EXTRA,  
				(ArrayList<? extends Parcelable>) meteos );
		startActivityForResult(intent, REQUEST_MAPS );
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if( requestCode == REQUEST_MAPS ){
			if( resultCode == RESULT_OK ){
				ISeance saved = (ISeance) intent.getExtras().get(ISeance.EXTRA_SEANCE_SAVED);
				seances.add(saved);
				findPerf(seances);
				Intent intentNext = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
    			intent.putExtra(ISeance.EXTRA_SEANCE, saved);
				startActivity(intent);
			}
		}
		
	}


	private class OnSeanceReceived implements IWebServiceReceiverArray{
		@Override
		public void onErrorResponse(VolleyError error) {
			String msg = "";
			switch (error.networkResponse.statusCode) {
			case 404:
				msg = "Unkown server (404)";
				break;
			case 500:
				msg = "Internal server error (500)";
				break;
			default:
				msg = "Unknow error";
				break;
			}
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
			finish();
		}

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(JSONArray seanceJson) {
			seances = (List<ISeance>) JsonSerializer.deserialize(Seance.class, seanceJson);
			ListSort.sortSeanceByDate(seances, SORT.DESC);
			loaded = true;
			
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.creationSeanceFragmentLayout);
			findPerf( seances );
			removeLoading(rl);
		}
	}
	
	private void findPerf( List<ISeance> list ){
		int dureeMax;
		float distanceMax;
		float vitesseMax;
		dureeMax = 0;
		distanceMax = 0;
		vitesseMax = 0;

		for(ISeance s : list ){
			if(s.getDuree() > dureeMax){
				chrono = s;
				dureeMax = s.getDuree();
			}
			if(s.getParcours().getDistance() / s.getDuree() > vitesseMax && s.getDuree() != 0 ) {
				speedy = s;
				vitesseMax = s.getParcours().getDistance() / s.getDuree();
			}
			if(s.getParcours().getDistance() > distanceMax) {
				farAway = s;
				distanceMax = s.getParcours().getDistance();
			}
		}
	}
	
	private void displayLoading(){
		InputStream stream = null; 
		try { 
			stream = getAssets().open("loader.gif"); 
		} catch (IOException e) {
			e.printStackTrace(); 
		}
		mGifView = new GifDecoderView(this, stream);
		
		mDialog = new Dialog(this);
		mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mDialog.setContentView(R.layout.activity_create_seance_loader);
		ViewGroup vg = (ViewGroup) mDialog.findViewById(R.id.activity_create_seance_loader_layout);
		vg.addView(mGifView);
		
		TextView txt = (TextView) mDialog.findViewById(R.id.activity_create_seance_loader_text);
		txt.setText("Chargement de votre profil...");
		
		mGifView.animate();
		mDialog.show();
	}
	
	private void removeLoading( ViewGroup layout )
	{
		mDialog.cancel();
	}

	@Override
	public void onItemClick(CarouselAdapter<?> parent, View view, int position,
			long id) {
		CarouselItem selection = (CarouselItem) mCarousel.getChildAt(position);
		if(selection.getName() == BEST_CHRONO){
			intent = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
			intent.putExtra(ISeance.EXTRA_SEANCE, chrono);
			startActivity(intent);
		} else if(selection.getName() == BEST_LENGTH){
			intent = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
			intent.putExtra(ISeance.EXTRA_SEANCE, farAway);
			startActivity(intent);
		} else if(selection.getName() == BEST_SPEED){
			intent = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
			intent.putExtra(ISeance.EXTRA_SEANCE, speedy);
			startActivity(intent);
		}
	}
	
	private class OnSportReceive implements IWebServiceReceiverArray
	{

		@Override
		public void onErrorResponse(VolleyError arg0) {
			Log.e("SWEATSHARE", "Error fetch sport.");
		}

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(JSONArray json) {
			sports = (List<ITypeSport>) JsonSerializer.deserialize(TypeSport.class, json);
			Log.d("SWEATSHARE", "NB sport fetch : "+ json.toString() );
		}
		
	}
	
	private class OnMeteoReceived implements IWebServiceReceiverArray
	{

		@Override
		public void onErrorResponse(VolleyError arg0) {
			Log.e("SWEATSHARE", "Error fetch meteo.");
		}

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(JSONArray json) {
			meteos = (List<IMeteo>) JsonSerializer.deserialize(Meteo.class, json);
			Log.d("SWEATSHARE", "NB meteo fetch : "+ json.toString() );
		}
		
	}
	
}
