package com.sweatshare.activities;

import org.json.JSONException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.sweatshare.R;
import com.sweatshare.communication.VolleySingleton;
import com.sweatshare.weather.api.JSONWeatherParser;
import com.sweatshare.weather.api.WeatherHttpClient;
import com.sweatshare.weather.model.Weather;

public class ShowMeteoActivity extends Activity implements 
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener{

	private TextView cityText;
	private TextView countryText;
	private TextView condDescr;
	private TextView temp;
	private TextView press;
	private TextView windSpeed;
	private TextView windDeg;
	private TextView hum;
	private ImageView imgView;
	private LocationClient mLocationClient;
	private Location mCurrentLocation;
	
	private Weather weather;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_meteo_activity);
		mLocationClient = new LocationClient(this, this, this);
		
		cityText = (TextView) findViewById(R.id.cityText);
		countryText = (TextView) findViewById(R.id.countryText);
		condDescr = (TextView) findViewById(R.id.condDescr);
		temp = (TextView) findViewById(R.id.temp);
		hum = (TextView) findViewById(R.id.hum);
		press = (TextView) findViewById(R.id.press);
		windSpeed = (TextView) findViewById(R.id.windSpeed);
		windDeg = (TextView) findViewById(R.id.windDeg);
		imgView = (ImageView) findViewById(R.id.condIcon);
		
		mLocationClient.connect();
	}
	
	private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {
		
		@Override
		protected Weather doInBackground(String... params) {
			weather = new Weather();
			String data = ( (new WeatherHttpClient()).getWeatherData(params[0],params[1]));
			Log.d("data", data);
			try {
				weather = JSONWeatherParser.getWeather(data);
				weather.iconData = ( (new WeatherHttpClient()).getImage(weather.currentCondition.getIcon()));
			} catch (JSONException e) {				
				e.printStackTrace();
			}
			
			return weather;
	}
		
	@Override
		protected void onPostExecute(Weather weather) {			
			super.onPostExecute(weather);
			
			VolleySingleton volley = VolleySingleton.getInstance();
			volley.getImageLoader().get("http://openweathermap.org/img/w/" + ShowMeteoActivity.this.weather.currentCondition.getIcon(),
				new ImageListener() {
				
				@Override
				public void onErrorResponse(VolleyError arg0) {
					
				}
				
				@Override
				public void onResponse(ImageContainer img, boolean arg1) {
					imgView.setImageBitmap(img.getBitmap());
				}
			});
			
			if (weather.iconData != null && weather.iconData.length > 0) {
				Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0, weather.iconData.length); 
				imgView.setImageBitmap(img);
			}
			countryText.setText(" " + weather.location.getCountry());
			cityText.setText(" " + weather.location.getCity());
			condDescr.setText(weather.currentCondition.getCondition() + " : " + weather.currentCondition.getDescr() + "");
			temp.setText("" + Math.round((weather.temperature.getTemp() - 273.15)) + "°C");
			hum.setText("" + weather.currentCondition.getHumidity() + "%");
			press.setText(" " + weather.currentCondition.getPressure() + " hPa");
			windSpeed.setText("" + weather.wind.getSpeed() + " mps");
			windDeg.setText("" + weather.wind.getDeg() + "°C");
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		mLocationClient.disconnect();
	}

	@Override
	public void onConnected(Bundle arg0) {
		mCurrentLocation = mLocationClient.getLastLocation();	
		JSONWeatherTask task = new JSONWeatherTask();
		task.execute(new String[]{String.valueOf(mCurrentLocation.getLongitude()),String.valueOf(mCurrentLocation.getLatitude())});
	}

	@Override
	public void onDisconnected() {
	}
}
