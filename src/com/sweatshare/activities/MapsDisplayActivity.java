/**
 * 
 */
package com.sweatshare.activities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sweatshare.R;
import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.IPointGPS;
import com.sweatshare.api.utils.ListSort;
import com.sweatshare.api.utils.ListSort.SORT;
import com.sweatshare.modele.Parcours;
import com.sweatshare.modele.PointGPS;

/**
 * @author Christophe
 * 
 */
public class MapsDisplayActivity extends FragmentActivity {

	private static final LatLng ISTIC = new LatLng(48.115339, -1.638580);

	private GoogleMap myMap; // map reference
	private IParcours parcours;
	
	// Manage the trace
	private PolylineOptions rectOptions;
	private Polyline polyline;
	private List<IPointGPS> listPointsGPS;
	private List<LatLng> listPointsTrace;

	// Manage the parameters of the display
	private double latMin = 0.0;
	private double latMax = 0.0;
	private double lngMin = 0.0;
	private double lngMax = 0.0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_display_maps);
		getMapReference();

		// Instantiates a new Polyline object and adds points to define a
		// rectangle
		rectOptions = new PolylineOptions().color(Color.BLUE);

		// Get back the mutable Polyline
		polyline = myMap.addPolyline(rectOptions);
		
		parcours = (Parcours) getIntent().getExtras().get(IParcours.EXTRA_PARCOURS);
		if (parcours != null && parcours.getGps() != null){
			this.setListPoints(parcours.getGps());
			this.displayTrace();
		}
		//testDisplay();
	}

	/**
	 * Activity's lifecycle event. onResume will be called when the Activity
	 * receives focus and is visible
	 */
	@Override
	protected void onResume() {
		super.onResume();
		getMapReference();
	}

	/**
	 * Activity's lifecycle event. onPause will be called when activity is going
	 * into the background,
	 */
	@Override
	public void onPause() {
		super.onPause();
	}

	/**
	 * Get a map object reference if none exits and enable blue arrow icon on
	 * map
	 */
	private void getMapReference() {
		if (myMap == null) {
			myMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.displaymap)).getMap();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void setUpMap() {
		// CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(ISTIC,
		// 15);
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(calculCentralPoint(), 14);
		myMap.animateCamera(cameraUpdate);

	}

	private LatLng calculCentralPoint() {
		double latitude = (latMax + latMin)/2;
		double longitude = (lngMax + lngMin)/2;
		return new LatLng(latitude, longitude);
	}

	private void displayTrace() {
		listPointsTrace = new ArrayList<LatLng>();
		//initialisation des min et max
		latMin = listPointsGPS.get(0).getX();
		latMax = listPointsGPS.get(0).getX();
		lngMin = listPointsGPS.get(0).getY();
		lngMax = listPointsGPS.get(0).getY();
		ListSort.sortGpsByDate(listPointsGPS, SORT.ASC);
		for (IPointGPS pointGPS : listPointsGPS) {
			LatLng point = new LatLng(pointGPS.getX(), pointGPS.getY());
			listPointsTrace.add(point);
			if (point.latitude > latMax)
				latMax = point.latitude;
			if (point.latitude < latMin)
				latMin = point.latitude;
			if (point.longitude > lngMax)
				lngMax = point.longitude;
			if (point.longitude < lngMin)
				lngMin = point.longitude;
		}
		polyline.setPoints(listPointsTrace);
		setUpMap();
	}

	/**
	 * @return the listPoints
	 */
	public List<IPointGPS> getListPoints() {
		return listPointsGPS;
	}

	/**
	 * @param listPoints
	 *            the listPoints to set
	 */
	public void setListPoints(Set<IPointGPS> listPoints) {
		this.listPointsGPS = new ArrayList<IPointGPS>();
		this.listPointsGPS.addAll(listPoints);
	}

}
