package com.sweatshare.activities;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.R;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverObject;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Utilisateur;

public class MainActivity extends Activity implements IWebServiceReceiverObject {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void sendRequest(View v)
	{	
		IWebService webService = WebService.getInstance(  );
		
		EditText pseudo = (EditText) findViewById(R.id.activity_main_pseudo);
		EditText passwd = (EditText) findViewById(R.id.activity_main_password);
		
		IUtilisateur utilisateur = new Utilisateur();
		utilisateur.setPseudo(pseudo.getText().toString());
		utilisateur.setPassword(passwd.getText().toString());
		webService.login(utilisateur, this);
	}
	
	public void goToSignIn(View v) {
		Intent userCreationIntent = new Intent(v.getContext(), CreateUserActivity.class);
        startActivityForResult(userCreationIntent, 0);
	}

	public void startActivityCreateUser(View v){
		Intent intent = new Intent(getApplicationContext(), CreateUserActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onResponse(JSONObject json) {
		IUtilisateur user = (IUtilisateur) JsonSerializer.deserialize(Utilisateur.class, json.toString());
		Intent intent = new Intent(this, HomeContainerActivity.class);
		intent.putExtra(IUtilisateur.USER_EXTRA, user);
		startActivity(intent);
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		String msg = "";
		Log.e("SWEATSHARE", "error : " + error.getMessage());
		if( error.networkResponse == null ){
			return;
		}
		switch (error.networkResponse.statusCode) {
		case 404:
			msg = "Unkown server (404)";
			break;
		case 500:
			msg = "Internal server error (500)";
			break;
		default:
			msg = "Unknow error";
			break;
		}
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}
	
}
