package com.sweatshare.activities;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.R;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverArray;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.list.adapater.SeanceBaseAdapter;
import com.sweatshare.modele.Seance;

public class ShowHistoryActivity extends Activity {

	private ListView maListViewPerso;
	private List<ISeance> seances;
	private IUtilisateur  user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_history_activity);
	
        maListViewPerso = (ListView) findViewById(R.id.list_view_history);
 
        seances = new ArrayList<ISeance>();
        
        user = (IUtilisateur) getIntent().getExtras().get(IUtilisateur.USER_EXTRA);
        
//        IWebService ws = WebService.getInstance();
//		ws.getSeances(user, new OnSeanceReceived());
//		
        this.seances = getIntent().getExtras().getParcelableArrayList( ISeance.EXTRA_SEANCE );
		displaySeance();
		
		maListViewPerso.setOnItemClickListener(new OnItemClickListener() {
			@Override
         	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
				Seance selected = (Seance) maListViewPerso.getItemAtPosition(position);
				Intent intent = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
    			intent.putExtra(ISeance.EXTRA_SEANCE, selected);
				startActivity(intent);
        	}
         });
    }
	
//	private class OnSeanceReceived implements IWebServiceReceiverArray{
//		@Override
//		public void onErrorResponse(VolleyError error) {
//			String msg = "";
//			switch (error.networkResponse.statusCode) {
//			case 404:
//				msg = "Unkown server (404)";
//				break;
//			case 500:
//				msg = "Internal server error (500)";
//				break;
//			default:
//				msg = "Unknow error";
//				break;
//			}
//			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
//		}
//
//		@SuppressWarnings("unchecked")
//		@Override
//		public void onResponse(JSONArray seanceJson) {
//			Log.d("JSON", seanceJson.toString());
//			ShowHistoryActivity.this.seances = (List<ISeance>) JsonSerializer.deserialize(Seance.class, seanceJson);
//			
//		}
//	}
	
	private void displaySeance(){
		
		SeanceBaseAdapter adapter = new SeanceBaseAdapter(getApplicationContext());
        if(seances!= null){
			for(int i = 0; i < seances.size(); i ++){
	        	ISeance s = seances.get(i);
	            adapter.add(s);
	        }
        }
        adapter.notifyDataSetChanged();
        maListViewPerso.setAdapter(adapter);
	}
}
