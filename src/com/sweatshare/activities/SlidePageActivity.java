/**
 * 
 */
package com.sweatshare.activities;

import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.sweatshare.R;
import com.sweatshare.api.page.transformer.DepthPageTransformer;
import com.sweatshare.page.LeftPageFragment;
import com.sweatshare.page.MiddlePageFragment;
import com.sweatshare.page.MyPagerAdapter;
import com.sweatshare.page.RightPageFragment;

/**
 * @author Christophe
 * 
 */
public class SlidePageActivity extends FragmentActivity {
	private PagerAdapter mPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.activity_slide_page);

		// Cr�ation de la liste de Fragments que fera d�filer le PagerAdapter
		List<Fragment> fragments = new Vector<Fragment>();

		// Ajout des Fragments dans la liste
		fragments.add(Fragment.instantiate(this,LeftPageFragment.class.getName()));
		fragments.add(Fragment.instantiate(this,MiddlePageFragment.class.getName()));
		fragments.add(Fragment.instantiate(this,RightPageFragment.class.getName()));

		// Cr�ation de l'adapter qui s'occupera de l'affichage de la liste de
		// Fragments
		this.mPagerAdapter = new MyPagerAdapter(super.getSupportFragmentManager(), fragments);

		ViewPager pager = (ViewPager) super.findViewById(R.id.viewpager);
		// Affectation de l'adapter au ViewPager
		pager.setAdapter(this.mPagerAdapter);
		
		pager.setPageTransformer(true, new DepthPageTransformer());
	}
}
