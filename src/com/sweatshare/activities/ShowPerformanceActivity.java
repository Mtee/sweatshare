package com.sweatshare.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.R;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverArray;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Seance;

public class ShowPerformanceActivity extends Activity {

	private ListView maListViewPerso;
	private IUtilisateur user;
	private ISeance speedy;
	private ISeance farAway;
	private ISeance chrono;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_performance_activity);
			
		user = (IUtilisateur) getIntent().getExtras().get(IUtilisateur.USER_EXTRA);
		
        maListViewPerso = (ListView) findViewById(R.id.list_view_performances);
        
		List<ISeance> seances = getIntent().getExtras().getParcelableArrayList(ISeance.EXTRA_SEANCE);
		findPerf( seances );
		displaySeances();
    }
	
	private void displaySeances() {
		ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        map = new HashMap<String, String>();
        map.put("img_perf", Integer.toString(R.drawable.speed));
        map.put("category", "Speedy");
        listItem.add(map);

        map = new HashMap<String, String>();
        map.put("img_perf", Integer.toString(R.drawable.parc));
        map.put("category", "Far Far Away");
        listItem.add(map);
 
        map = new HashMap<String, String>();
        map.put("img_perf", Integer.toString(R.drawable.chrono));
        map.put("category", "Chrono");
        listItem.add(map);
 
        SimpleAdapter mSchedule = new SimpleAdapter (this.getBaseContext(), listItem, R.layout.performances_list_view,
               new String[] {"img_perf", "category"}, new int[] {R.id.img_perf, R.id.category});
        maListViewPerso.setAdapter(mSchedule);
        maListViewPerso.setOnItemClickListener(new OnItemClickListener() {
			@Override
        	@SuppressWarnings("unchecked")
         	public void onItemClick(AdapterView<?> a, View v, int position, long id) {
				HashMap<String, String> map = (HashMap<String, String>) maListViewPerso.getItemAtPosition(position);
				boolean ret = true;
				ISeance selected = null;
				if(map.get("category") == "Chrono" && chrono != null){
					selected = chrono;
        		} else if (map.get("category") == "Speedy" && speedy != null){
        			selected = speedy;
        		} else if (map.get("category") == "Far Far Away" && farAway != null){
        			selected = farAway;
        		} else {
        			AlertDialog.Builder adb = new AlertDialog.Builder(ShowPerformanceActivity.this);
        			adb.setMessage(" il n'y a pas de courses valables ou une erreur de connexion est survenue.");
        			adb.setPositiveButton("Ok", null);
            		adb.show();
            		ret = false;
        		}
        		if (ret && selected != null) {
        			intent = new Intent(getApplicationContext(),ShowSeanceDetailsActivity.class );
        			intent.putExtra(ISeance.EXTRA_SEANCE, selected);
					startActivity(intent);
        		}
        	}
         });
	}
	

	private void findPerf( List<ISeance> list ){
		int dureeMax;
		float distanceMax;
		float vitesseMax;
		dureeMax = 0;
		distanceMax = 0;
		vitesseMax = 0;

		for(ISeance s : list ){
			if(s.getDuree() > dureeMax){
				chrono = s;
				dureeMax = s.getDuree();
			}
			if(s.getParcours().getDistance() / s.getDuree() > vitesseMax && s.getDuree() != 0 ) {
				speedy = s;
				vitesseMax = s.getParcours().getDistance() / s.getDuree();
			}
			if(s.getParcours().getDistance() > distanceMax) {
				farAway = s;
				distanceMax = s.getParcours().getDistance();
			}
		}
		displaySeances();
	}
}
