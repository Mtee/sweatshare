/**
 * 
 */
package com.sweatshare.activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.json.JSONObject;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sweatshare.R;
import com.sweatshare.api.communication.IWebServiceReceiverObject;
import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.IPointGPS;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.api.utils.LocationUtils;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Meteo;
import com.sweatshare.modele.Parcours;
import com.sweatshare.modele.PointGPS;
import com.sweatshare.modele.Seance;

/**
 * @author Christophe
 * 
 */
public class MapsTrackingActivity extends FragmentActivity implements
		LocationListener, GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private static final LatLng ISTIC = new LatLng(48.115339, -1.638580);

	private GoogleMap myMap; // map reference
	private LocationClient myLocationClient;
	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(5000) // 5 seconds
			.setFastestInterval(16) // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	// Chrono
	Chronometer chronometer;
	
	// Pause time global
	private long totalPauseTime = 0L;
	
	// Last pause time
	private Date lastPauseTime;
	
	// Manage trace
	private PolylineOptions rectOptions;
	private Polyline polyline;
	private List<LatLng> listPointsTrace;

	private List<Location> listLocation;

	private List<ITypeSport> typeSport;
	private List<IMeteo> meteos;
	
	// Start Stop
	private Boolean isStarted;
	
	private Date dateStarted;
	private long time;
	private IUtilisateur utilisateur;
	private ISeance seanceSaved;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_tracking_maps);
		getMapReference();

		// Instantiates a new Polyline object and adds points to define a
		// rectangle
		rectOptions = new PolylineOptions().color(Color.BLUE);

		// Get back the mutable Polyline
		polyline = myMap.addPolyline(rectOptions);

		listLocation = new ArrayList<Location>();
		isStarted = false;

		// D�marrage du chrono
		this.initChrono();

		typeSport = getIntent().getExtras().getParcelableArrayList(ITypeSport.TYPESPORT_EXTRA);
		meteos = getIntent().getExtras().getParcelableArrayList(IMeteo.METEO_EXTRA);
		utilisateur = (IUtilisateur) getIntent().getExtras().get(IUtilisateur.USER_EXTRA);
		
	}

	/**
	 * Activity's lifecycle event. onResume will be called when the Activity
	 * receives focus and is visible
	 */
	@Override
	protected void onResume() {
		super.onResume();
		getMapReference();
		wakeUpLocationClient();
		// chronometer.start();
		myLocationClient.connect();
		setUpMap();
	}

	private void setUpMap() {
		/*
		 * CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom( ISTIC,
		 * 14); myMap.animateCamera(cameraUpdate);
		 */
	}

	public void startRecord(View view) {
		this.isStarted = true;
		dateStarted = new Date();
		this.startChrono();
	}


	public void stopRecord(View view) {
		this.isStarted = false;
		stopChrono();
		save();
	}

	/**
	 * 
	 * @param lat
	 *            - latitude of the location to move the camera to
	 * @param lng
	 *            - longitude of the location to move the camera to Prepares a
	 *            CameraUpdate object to be used with callbacks
	 */
	private void gotoMyLocation(Location location) {
		CameraUpdate cameraUpdate = CameraUpdateFactory
				.newLatLngZoom(
						new LatLng(location.getLatitude(), location
								.getLongitude()), 14);
		myMap.animateCamera(cameraUpdate);
		if (isStarted) {
			saveLocation(location);
			addPointToTrace(location.getLatitude(), location.getLongitude());
		}

	}

	private void addPointToTrace(double lat, double lng) {
		listPointsTrace = polyline.getPoints();
		listPointsTrace.add(new LatLng(lat, lng));
		polyline.setPoints(listPointsTrace);
	}

	private void saveLocation(Location location) {
		// Sauvegarde de la location
		listLocation.add(location);
	}

	/**
	 * @return the listPoints
	 */
	public List<IPointGPS> getListPoints() {
		
		List<IPointGPS> listPoints = new ArrayList<IPointGPS>();

		for (Location point : listLocation) {
			IPointGPS pointGPS = new PointGPS();
			pointGPS.setDateReleve(new Date(point.getTime()));
			pointGPS.setX((float) point.getLatitude());
			pointGPS.setY((float) point.getLongitude());
			pointGPS.setZ((float) point.getAltitude());
			// pointGPS.setSpeed((float) point.getSpeed());
			listPoints.add(pointGPS);
		}
		return listPoints;
	}

	/**
	 * When we receive focus, we need to get back our LocationClient Creates a
	 * new LocationClient object if there is none
	 */
	private void wakeUpLocationClient() {
		if (myLocationClient == null) {
			myLocationClient = new LocationClient(getApplicationContext(),
					this, // Connection Callbacks
					this); // OnConnectionFailedListener
		}
	}

	/**
	 * Get a map object reference if none exits and enable blue arrow icon on
	 * map
	 */
	private void getMapReference() {
		if (myMap == null) {
			myMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.trackingmap)).getMap();
		}
		if (myMap != null) {
			myMap.setMyLocationEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// //////////////////////////////////
	// Check Google play services

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d(LocationUtils.APPTAG,
					getString(R.string.play_services_available));
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getFragmentManager(), LocationUtils.APPTAG);
			}
			return false;
		}
	}

	/*
	 * Called by Location Services if the attempt to Location Services fails.
	 */
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

		/*
		 * Google Play services can resolve some errors it detects. If the error
		 * has a resolution, try sending an Intent to start a Google Play
		 * services activity that can resolve error.
		 */
		if (connectionResult.hasResolution()) {
			try {

				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */

			} catch (IntentSender.SendIntentException e) {

				// Log the error
				e.printStackTrace();
			}
		} else {

			// If no resolution is available, display a dialog to the user with
			// the error.
			showErrorDialog(connectionResult.getErrorCode());
		}
	}

	/**
	 * Show a dialog returned by Google Play services for the connection error
	 * code
	 * 
	 * @param errorCode
	 *            An error code returned from onConnectionFailed
	 */
	private void showErrorDialog(int errorCode) {

		// Get the error dialog from Google Play services
		Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode,
				this, LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

		// If Google Play services can provide an error dialog
		if (errorDialog != null) {

			// Create a new DialogFragment in which to show the error dialog
			ErrorDialogFragment errorFragment = new ErrorDialogFragment();

			// Set the dialog in the DialogFragment
			errorFragment.setDialog(errorDialog);

			// Show the error dialog in the DialogFragment
			errorFragment.show(getFragmentManager(), LocationUtils.APPTAG);
		}
	}

	/**
	 * Define a DialogFragment to display the error dialog generated in
	 * showErrorDialog.
	 */
	public static class ErrorDialogFragment extends DialogFragment {

		// Global field to contain the error dialog
		private Dialog mDialog;

		/**
		 * Default constructor. Sets the dialog field to null
		 */
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		/**
		 * Set the dialog to display
		 * 
		 * @param dialog
		 *            An error dialog
		 */
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		/*
		 * This method must return a Dialog to the DialogFragment.
		 */
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	/**
	 * 
	 * @param bundle
	 *            LocationClient is connected
	 */
	@Override
	public void onConnected(Bundle bundle) {
		myLocationClient.requestLocationUpdates(REQUEST, this); // LocationListener
	}

	/**
	 * LocationClient is disconnected
	 */
	@Override
	public void onDisconnected() {

	}

	@Override
	public void onLocationChanged(Location location) {
		gotoMyLocation(location);

	}

	private void changeCamera(CameraUpdate update,
			GoogleMap.CancelableCallback callback) {
		myMap.moveCamera(update);
	}

	/**
	 * M�thode qui d�marre le chronom�tre
	 */
	private void startChrono() {
		
		Log.d("toto2", " test : " + (this.lastPauseTime == null));
		
		if (this.lastPauseTime == null) {
			
			Log.d("toto2", "first time");
			chronometer.setBase(this.dateStarted.getTime());
			chronometer.start();
			
		}
		else
		{
			
			Log.d("toto2", "after pause");
			long timeBreak = new Date().getTime() - lastPauseTime.getTime();
			totalPauseTime += timeBreak;
			chronometer.start();
			
		}
	}
	
	public void pauseRecord(View view) {
		this.isStarted = true;
		this.lastPauseTime = new Date();
		this.chronometer.stop();
	}

	
	/**
	 * Activity's lifecycle event. onPause will be called when activity is going
	 * into the background,
	 */
	@Override
	public void onPause() {
		super.onPause();
		// chronometer.stop();
		if (myLocationClient != null) {
			myLocationClient.disconnect();
		}
	}
	
	/**
	 * M�thode qui stop le chronom�tre
	 */
	private void stopChrono() {
		this.time = (new Date().getTime() - chronometer.getBase()) - totalPauseTime;	
		Log.d("toto2", this.time + "");
		chronometer.stop();
	}

	/**
	 * M�thode qui initialise le chronom�tre
	 */
	private void initChrono() {
		
		chronometer = (Chronometer) this.findViewById(R.id.fragment_map_chrono);
		chronometer.setOnChronometerTickListener(new OnChronometerTickListener() {
					
			@Override
				public void onChronometerTick(Chronometer cArg) {
					
					long t = (new Date().getTime() - cArg.getBase() - (1000 * 60 * 60)) - totalPauseTime;	 			
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
					cArg.setText( sdf.format(new Date(t)) );
					
				}
			});
	}
	
	public ITypeSport findSportByLibelle( String type )
	{
		for( ITypeSport ts : this.typeSport ){
			if( ts.getLibelle().equals( type ) ){
				return ts;
			}
		}
		return null;
	}
	
	public IMeteo findMeteoByLibelle( String libelle ){
		for(IMeteo meteo : this.meteos){
			if( meteo.getLibelle().equals( libelle ) ){
				return meteo;
			}
		}
		return null;
	}
	
	public void save(){
		
		seanceSaved = new Seance();
		Date dateEnd = new Date();
		
		IParcours p = new Parcours();
		p.setPointGps(new HashSet<IPointGPS>(getListPoints()));
		seanceSaved.setParcours( p );
		Float distance = (float) 0;
		for (int i=0;i<listLocation.size()-1;i++){
			distance += listLocation.get(i).distanceTo(listLocation.get(i+1));
		}
		p.setDistance(distance);
		seanceSaved.setDate( dateStarted );
		seanceSaved.setDuree( (int) ((time/1000))  );
		//seanceSaved.setDuree((int)600);
		seanceSaved.setHeureArrivee( dateEnd );
		seanceSaved.setHeureDepart(dateStarted);
		seanceSaved.setSport( findSportByLibelle("running") );
		seanceSaved.setUtilisateur( utilisateur );
		seanceSaved.setMeteo( findMeteoByLibelle("sun") );
		WebService ws = WebService.getInstance();
		ws.createSeance(utilisateur, seanceSaved, new OnSeanceCreated());
	}
	
	private class OnSeanceCreated implements IWebServiceReceiverObject
	{
		@Override
		public void onErrorResponse(VolleyError arg0) {
			Log.e("SWEATSHARE", "Failed to create seance.");
		}
		@Override
		public void onResponse(JSONObject arg0) {
			Intent intent = new Intent(getApplicationContext(), ShowSeanceDetailsActivity.class);
			intent.putExtra( IUtilisateur.USER_EXTRA , utilisateur);
			intent.putExtra(ISeance.EXTRA_SEANCE_SAVED, seanceSaved);
			setResult(RESULT_OK, intent);
			finish();
		}
		
	}
	
}
