package com.sweatshare.activities;

import java.util.Date;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.HomeActivity;
import com.sweatshare.R;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverObject;
import com.sweatshare.api.modele.IBooleanResponse;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Utilisateur;
import com.sweatshare.modele.WebServiceBooleanResponse;

public class CreateUserActivity extends Activity implements IWebServiceReceiverObject {

	private IUtilisateur utilisateur;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_user);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onErrorResponse(VolleyError arg0) {
		Log.e("SWEATSHARE", "Failed to create user");
		
	}

	@Override
	public void onResponse(JSONObject json) {
		Log.d("Received",json.toString());
		
//		IUtilisateur utilisateur = (IUtilisateur) JsonSerializer.deserialize(Utilisateur.class, json.toString());
		IBooleanResponse response = (IBooleanResponse) JsonSerializer.deserialize(WebServiceBooleanResponse.class, json.toString());
		if( response.getResult() ){
			WebService ws = WebService.getInstance();
			ws.login(utilisateur, new onUserLogin());
		}else{
			Toast.makeText(this, "Failed to create user", Toast.LENGTH_LONG).show();
		}
	}
	
	public void register(View v)
	{
		utilisateur = new Utilisateur();
		EditText txt = (EditText) findViewById(R.id.create_user_activity_lastname);
		utilisateur.setNom( txt.getText().toString() );
		
		txt = (EditText) findViewById(R.id.create_user_activity_firstname);
		utilisateur.setPrenom( txt.getText().toString() );
		
		txt = (EditText) findViewById(R.id.create_user_activity_password);
		utilisateur.setPassword( txt.getText().toString() );
		
//		txt = (EditText) findViewById(R.id.create_user_activity_birthday);
//		utilisateur.setDateNaissance( txt.getText().toString() );
		
		DatePicker birthday = (DatePicker) findViewById(R.id.create_user_activity_birthday);
		utilisateur.setDateNaissance( new Date(birthday.getCalendarView().getDate()) );
		
		txt = (EditText) findViewById(R.id.create_user_activity_pseudo);
		utilisateur.setPseudo( txt.getText().toString() );
		
		txt = (EditText) findViewById(R.id.create_user_activity_weight);
		utilisateur.setPoids( Float.parseFloat(txt.getText().toString()) );
		
		txt = (EditText) findViewById(R.id.create_user_activity_size);
		utilisateur.setTaille( Integer.parseInt(txt.getText().toString()) );
		
		IWebService webService = WebService.getInstance();
		webService.createUser(utilisateur, this);
	}
	
	private class onUserLogin implements IWebServiceReceiverObject{

		@Override
		public void onErrorResponse(VolleyError arg0) {
			Toast.makeText(getApplicationContext(), "Bad user creation", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onResponse(JSONObject json) {
			Intent intent = new Intent( getApplicationContext(), HomeActivity.class );
			IUtilisateur user = (IUtilisateur) JsonSerializer.deserialize(Utilisateur.class, json.toString());
			intent.putExtra(IUtilisateur.USER_EXTRA, user);
			startActivity(intent);
			Toast.makeText(getApplicationContext(), "User created successfully", Toast.LENGTH_LONG).show();
		}
		
	}

}
