package com.sweatshare.activities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.sweatshare.R;
import com.sweatshare.SweatShareApplication;
import com.sweatshare.api.modele.IParcours;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.communication.VolleySingleton;
import com.sweatshare.communication.WebService;

public class ShowSeanceDetailsActivity extends Activity {

	private static final String TAG = ShowSeanceDetailsActivity.class.getName();

	private ISeance seance;

	private TextView duree;
	private TextView dateDep;
	private TextView dateArr;
	private TextView vitesse;
	private TextView meteo;
	private TextView date;
	private TextView sport;
	private TextView distance;
	private UiLifecycleHelper uiHelper;
	private ImageButton publish;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			Log.i(TAG, state.name());
			if (session.isOpened())
				toast("T'es logg� a Facebook mec !");
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_seance_details);

		seance = (ISeance) getIntent().getExtras().get(ISeance.EXTRA_SEANCE);
		if (seance != null) {
			initData();
		}

		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);

		publish = (ImageButton) findViewById(R.id.ButtonShareFacebook);
		ConfigPublishFeed();

    }
	
	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		uiHelper.onActivityResult(requestCode, resultCode, data,
				new FacebookDialog.Callback() {

					@Override
					public void onError(PendingCall pendingCall,
							Exception error, Bundle data) {
						Log.e(TAG, String.format("Error: %s", error.toString()));
					}

					@Override
					public void onComplete(PendingCall pendingCall, Bundle data) {
						if (FacebookDialog.getNativeDialogDidComplete(data)) {
							toast("Publish success !");
						} else {
							toast("Publish cancelled !");
						}
					}
				});
	}

	private void ConfigPublishFeed() {
		publish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				publishFeed();
			}
		});
	}

	private void publishFeed() {
		if (FacebookDialog.canPresentShareDialog(
				SweatShareApplication.getAppContext(),
				FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
			// Publish the post using the Share Dialog
			String desc = "J'ai courru " + seance.getParcours().getDistance()
					+ " metres en " + seance.getDuree() + " min";
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(
					this)
					.setName(desc)
					.setDescription("C'est cool non !")
					.setPicture(
							"http://img15.hostingpics.net/pics/271155iclauncher.png")
					.setLink("http://buildandrun.aws.af.cm/#/login").build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else {
			// Publish the post using the Feed Dialog
			Bundle params = new Bundle();
			params.putString("name", "SweatShare");
			params.putString("caption", "Cool");
			params.putString("description", "Description");
			params.putString("link", "http://buildandrun.aws.af.cm/#/login");
			params.putString("picture",
					"http://www.sirtin.fr/sirtin/wp-content/uploads/110401-02.jpg");

			WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(
					SweatShareApplication.getAppContext(),
					Session.getActiveSession(), params)).setOnCompleteListener(
					new OnCompleteListener() {

						@Override
						public void onComplete(Bundle values,
								FacebookException error) {
							if (error == null) {
								// When the story is posted, echo the success
								// and the post Id.
								final String postId = values
										.getString("post_id");
								if (postId != null) {
									toast("Posted story, id: " + postId);
								} else {
									// User clicked the Cancel button
									toast("Publish cancelled");
								}
							} else if (error instanceof FacebookOperationCanceledException) {
								// User clicked the "x" button
								toast("Publish cancelled");
							} else {
								// Generic, ex: network error
								toast("Error posting story");
							}
						}
					}).build();
			feedDialog.show();
		}
	}

	/**
	 * Show toast
	 * 
	 * @param message
	 */
	private void toast(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
				.show();
	}

	private void initData() {

		DateFormat df = DateFormat.getDateInstance();
		DateFormat df2 = new SimpleDateFormat("HH:mm:ss");

		duree = (TextView) findViewById(R.id.dureeText);
		date = (TextView) findViewById(R.id.dateTextDetails);
		dateDep = (TextView) findViewById(R.id.dateDepDetails);
		dateArr = (TextView) findViewById(R.id.dateArrDetails);
		vitesse = (TextView) findViewById(R.id.vitesseDetails);
		meteo = (TextView) findViewById(R.id.meteoDetails);
		sport = (TextView) findViewById(R.id.sportDetails);
		distance = (TextView) findViewById(R.id.distanceDetails);
		float temps = 0;
		if (seance.getDuree() != null)
			temps = ((float)seance.getDuree()) / 60;
		duree.setText(temps + " minutes");
		if (seance.getDate() != null)
			date.setText(df.format(seance.getDate()));
		if (seance.getHeureArrivee() != null)
			dateArr.setText(df2.format(seance.getHeureArrivee()));
		if (seance.getHeureDepart() != null)
			dateDep.setText(df2.format(seance.getHeureDepart()) + " ");
		if (seance.getParcours() != null)
			distance.setText(Float.toString(seance.getParcours().getDistance())
					+ " meters");
		float dist = seance.getParcours().getDistance() / 1000;
		float speed = 0;
		float res = 0;
		if (seance.getDuree() != null) {
			speed = ((float) temps) / 60;
			Log.v("SWEAT", dist + " " + speed);
			res = dist / speed;
		}
		vitesse.setText(Float.toString(res) + " Km/h");

		ImageView img = null;
		VolleySingleton volley = VolleySingleton.getInstance();

		if (seance.getMeteo() != null) {
			meteo.setText(seance.getMeteo().getLibelle());
			img = (ImageView) findViewById(R.id.show_seance_details_meteo_img);
			volley.getImageLoader().get(buildUrlMeteo(),
					new ImageSeanceLoader(img));
		}
		if (seance.getSport() != null) {
			sport.setText(" " + seance.getSport().getLibelle());
			img = (ImageView) findViewById(R.id.show_seance_details_sport_img);
			volley.getImageLoader().get(buildUrlSport(),
					new ImageSeanceLoader(img));
		}
	}

	public void showTrack(View v) {
		if (seance.getParcours().getGps() != null
				&& seance.getParcours().getGps().size() > 0) {
			Intent intent = new Intent(getApplicationContext(),
					MapsDisplayActivity.class);
			intent.putExtra(IParcours.EXTRA_PARCOURS, seance.getParcours());
			startActivity(intent);
		} else {
			Toast.makeText(getApplicationContext(),
					"this is a test track, no view on map available",
					Toast.LENGTH_LONG).show();
		}
	}

	private class ImageSeanceLoader implements ImageListener {

		private ImageView img;

		public ImageSeanceLoader(ImageView img) {
			this.img = img;
		}

		@Override
		public void onErrorResponse(VolleyError arg0) {
			img.setImageResource(R.drawable.error_connexion);
			Log.e("SWEATSHARE", arg0.getMessage() + "");
		}

		@Override
		public void onResponse(ImageContainer container, boolean res) {
			img.setImageBitmap(container.getBitmap());
		}
	}

	private String buildUrlSport() {
		return WebService.URL_IMAGES + seance.getSport().getLibelle() + ".png";
	}

	private String buildUrlMeteo() {
		return WebService.URL_IMAGES + seance.getMeteo().getLibelle() + ".png";
	}

}
