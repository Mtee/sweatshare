package com.sweatshare;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sweatshare.api.communication.IWebService;
import com.sweatshare.api.communication.IWebServiceReceiverArray;
import com.sweatshare.api.communication.IWebServiceReceiverObject;
import com.sweatshare.api.modele.IMeteo;
import com.sweatshare.api.modele.ISeance;
import com.sweatshare.api.modele.ITypeSport;
import com.sweatshare.api.modele.IUtilisateur;
import com.sweatshare.communication.JsonSerializer;
import com.sweatshare.communication.WebService;
import com.sweatshare.modele.Meteo;
import com.sweatshare.modele.Seance;
import com.sweatshare.modele.TypeSport;

public class HomeActivity extends Activity implements IWebServiceReceiverArray {

	private List<IMeteo> meteos;
	private List<ITypeSport> sports;
	private IUtilisateur user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		meteos = new ArrayList<IMeteo>();
		sports = new ArrayList<ITypeSport>();
		
//		IWebService webService = WebService.getInstance(getApplicationContext());
		IWebService webService = WebService.getInstance();
		
		user = (IUtilisateur) getIntent().getExtras().get(IUtilisateur.USER_EXTRA);
		
		//load meteo :
		getMeteo();
		
		//Load available sports :
		getSport();
		
		//Load seances :
		webService.getSeances(user, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	public void createSeance(View v){
		Date current = new Date();
		ISeance seance = new Seance();
		seance.setDate( current );
		seance.setDuree(60);
		seance.setHeureArrivee( new Date(current.getTime() + (60*3600*1000))  );
		seance.setHeureDepart( current );
		
		if( meteos != null && meteos.size() > 0 ){
			seance.setMeteo(meteos.get(0));
		}
		
		seance.setUtilisateur(user);
		
		IWebService ws = WebService.getInstance();
		ws.createSeance(user, seance, new OnSeanceCreated());
		
	}
	
	public void getMeteo( View v ){
		getMeteo();
	}
	
	public void getMeteo(){
		IWebService ws = WebService.getInstance();
		ws.getAvailableMeteos(new OnMeteosReceived());
	}
	
	public void getSport(){
		IWebService ws = WebService.getInstance();
		ws.getAvailableSports(new OnSportsReceived());
	}
	
	private class OnMeteosReceived implements IWebServiceReceiverArray{
		@Override
		public void onErrorResponse(VolleyError error) {
			String msg = "";
			switch (error.networkResponse.statusCode) {
			case 404:
				msg = "Unkown server (404)";
				break;
			case 500:
				msg = "Internal server error (500)";
				break;
			default:
				msg = "Unknow error";
				break;
			}
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		}

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(JSONArray meteoJson) {
			HomeActivity.this.meteos = (List<IMeteo>) JsonSerializer.deserialize(Meteo.class, meteoJson);
			for(IMeteo meteo : meteos ){
				Log.i("SWEATSHARE", meteo.getLibelle());
			}
		}
		
	}
	
	private class OnSportsReceived implements IWebServiceReceiverArray{

		@Override
		public void onErrorResponse(VolleyError error) {
			String msg = "";
			switch (error.networkResponse.statusCode) {
			case 404:
				msg = "Unkown server (404)";
				break;
			case 500:
				msg = "Internal server error (500)";
				break;
			default:
				msg = "Unknow error";
				break;
			}
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		}

		@SuppressWarnings("unchecked")
		@Override
		public void onResponse(JSONArray jsonSport) {
			HomeActivity.this.sports = (List<ITypeSport>) JsonSerializer.deserialize(TypeSport.class, jsonSport);
			for(ITypeSport sport : sports ){
				Log.i("SWEATSHARE", sport.getLibelle());
			}
		}
		
	}
	
	private class OnSeanceCreated implements IWebServiceReceiverObject{

		@Override
		public void onErrorResponse(VolleyError error) {
			String msg = "";
			if( error.networkResponse == null ){
				Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
				return;
			}
			switch (error.networkResponse.statusCode) {
			case 404:
				msg = "Unkown server (404)";
				break;
			case 500:
				msg = "Internal server error (500)";
				break;
			default:
				msg = "Unknow error";
				break;
			}
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		}


		@Override
		public void onResponse(JSONObject response) {
			Log.d("SWEATSHARE - create seance response", response.toString());
		}
		
	}
	
	@Override
	public void onErrorResponse(VolleyError arg0) {
		Log.e("SWEATSHARE", "Failed to get seances");
		Log.d("SWEATSHARE", arg0.toString());
	}

	@Override
	public void onResponse(JSONArray json) {
		Log.i("SWEATSHARE", "SEANCE get obtained successfully " + json.toString() );
		@SuppressWarnings("unchecked")
		List<ISeance> seances = (List<ISeance>) JsonSerializer.deserialize(Seance.class, json);
		for( ISeance s : seances ){
			Log.d("SWEATSHARE", "Seance : " + s.getDate() );
			Log.d("SWEATSHARE", "Seance : " + s.getDuree() );
		}
	}

}
